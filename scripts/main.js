/*

Питання 1. Описати своїми словами навіщо потрібні функції у програмуванні.

Відповідь 1. В разі потреби виконувати якусь частину коду в продовж програми декілька разів, такий код можна оформити у вигляді функції та викликати його 
            у потрібному місці через виклик функції. 

Питання 2. Описати своїми словами, навіщо у функцію передавати аргумент.

Відповідь 2. Аргументи використовуються функцією для її реалізації з урахуванням вказаних у аргументах даних. Т.ч. функція для своєї реалізації використовує
            ці дані (значення), що передаються в неї через аргументи. 

Питання 3. Що таке оператор return та як він працює всередині функції?

Відповідь 3. Oператор return повертає результат функції, який можна використовувати зовні самої функції.  

*/

"use strict"; //strict mode is a way to introduce better error-checking into your code 


let num1; 
let num2;

while (!num1 || isNaN(num1)) num1 = +prompt("Enter number #1:", "");
console.log(num1, typeof(num1));

while (!num2 || isNaN(num2)) num2 = +prompt("Enter number #2:", "");
console.log(num2, typeof(num2));

let mathOperation = prompt('Enter one of the mathematical operations "/ , * , - , +" : ', "+"); 
console.log(mathOperation, typeof(mathOperation));


function calcResult(){
      let result; 
      switch(mathOperation) {
            case "/":
                  result = num1 / num2;
                  break;
            case "*":
                  result = num1 * num2;
                  break;
            case "-":
                  result = num1 - num2;
                  break;
            case "+":
                  result = num1 + num2;
                  break;
            default: 
                  alert("You entered an invalid mathematical operator"); 
      }
      // console.log(result);
      return result;
}

// calcResult();

console.log(calcResult());
